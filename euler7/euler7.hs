-- which is faster???
clist1 :: Integer -> [Integer]
clist1 1 = [1]
clist1 n = (clist1 (n-1)) ++ [n]

clist2 :: Integer -> [Integer]
clist2 1 = [1]
clist2 n = n:[n | n <- clist2 (n-1)]

factorize 1 = []
factorize 2 = []
factorize n = if (mod n 2 == 0)
                 then [2]
                 else factorizeHelper n 3 []

factorizeHelper :: Integer -> Integer -> [Integer] -> [Integer]
factorizeHelper x y z = if (x <= (y*2))
                           then z
                           else if (mod x y == 0)
                                   then factorizeHelper x (y+2) (y:z)
                                   else factorizeHelper x (y+2) z

isPrime n = if (factorize n == [])
               then True
               else False
               
primeList n 10002 = [(0,0)]
primeList n count = if (isPrime n)
                       then (count, n):(primeList (n+1) (count+1))
                       else primeList (n+1) count

-- method 1
primerator :: Integer -> [Integer] -> [Integer] -> Integer -> Integer
primerator n d p 10001 = (last p)
primerator n d p c = if (d == [])
                        then primerator (n+2) (p++[n]) (p++[n]) (c+1)
                        else if (mod n (head d) == 0)
                                then primerator (n+2) p p c
                                else primerator n (tail d) p c
                 

solve7method1 = primerator 3 [2] [2] 1
solve7method2 = primeList 38273 4039