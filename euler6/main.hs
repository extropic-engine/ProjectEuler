square :: Integer -> Integer
square n = n*n

squares :: [Integer] -> [Integer]
squares xs = [ square x | x <- xs ]

clist :: Integer -> [Integer]
clist 1 = [1]
clist n = n:[n | n <- clist (n-1)]

listsum :: [Integer] -> Integer
listsum [] = 0
listsum n = (head n) + listsum(tail n)

solve6 n = (listsum(squares(clist n))) - (square(listsum(clist n)))