#include <stdio.h>

bool divisibleby20(int x);

int main() {
    int i = 2;
    while (i > 0) {
        if (divisibleby20(i))
            fprintf(stdout, "%d\n", i);
        i++;
    }
    return 0;
}

bool divisibleby20(int x) {
    for (int i = 2; i <= 20; i++) {
        if (x % i != 0)
            return false;
    }
    return true;
}
