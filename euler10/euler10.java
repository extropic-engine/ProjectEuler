import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


public class euler10 {

	private static final int TWO_MILLION = 2000000;
	
	public static void main(String[] args) {
		
		List<Integer> primes = new ArrayList<Integer>();
		
		for (int i = 2; i < TWO_MILLION; i++) {
			boolean isPrime = true;
			for (int p : primes) {
				if (i % p == 0) {
					isPrime = false;
					break;
				}
			}
			if (isPrime) {
				//System.out.println(i + " is prime.");
				primes.add(i);
			}
		}
		
		BigInteger sum = new BigInteger("0");
		for (Integer p : primes) {
			sum = sum.add(new BigInteger(p.toString()));
		}
		System.out.println("The sum of all primes below " + TWO_MILLION + " is " + sum);
	}
}
