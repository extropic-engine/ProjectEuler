
public class euler9 {

	public static void main(String[] args) {
		
		for (int a = 0; a < 1000; a++) {
			int b = a + 1;
			while (a+b < 1000) {
				int c = 1000 - (b+a);
				if (a+b+c == 1000 && (a*a) + (b*b) == (c*c)) {
						System.out.println("A: " + a + " B: " + b + " C: " + c);
				}
				b++;
			}
		}
	}
}