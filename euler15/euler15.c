#include <stdio.h>

unsigned long cache[21][21];

int main() {
  for (int i = 0; i < 21; i++) {
      cache[i][0] = 1;
      cache[0][i] = 1;
  }
  for (int i = 1; i < 21; i++) {
    for (int m = 1; m < 21; m++) {
      cache[i][m] = cache[i-1][m] + cache[i][m-1];
      printf("position (%d, %d) has %lu routes\n", i, m, cache[i][m]);
    }
  }
}
