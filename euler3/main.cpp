bool is_palindrome(int x);
int pow(int x, int y);
int count_digits(int x);

#include <stdio.h>

int main() {
    is_palindrome(98589);
    is_palindrome(655);
    int d1 = 999;
    int d2 = 999;
    int biggest = 1;
    for (int i = 999; i >= 100; i--) {
        for (int m = 999; m >= 100; m--) {
            int x = m*i;
            if (is_palindrome(x) && x > biggest)
                biggest = x;
        }
    }
    fprintf(stdout, "%d", biggest);
    return 0;
}

bool is_palindrome(int x) {
    int d = count_digits(x);
    for (int i = d; i >= 1; i--) {
        int l = (x % pow(10, i)) / pow(10, i-1);
        int r = (x % pow(10, (d - i) + 1)) / pow(10, (d - i));
        if (l != r)
            return false;
    }
    return true;
}

int pow(int x, int y) {
    if (y == 0)
        return 1;
    if (y == 1)
        return x;
    return pow(x, y-1) * x;
}

int count_digits(int x) {
    if (x >= 100000)
        return 6;
    if (x >= 10000)
        return 5;
    if (x >= 1000)
        return 4;
    if (x >= 100)
        return 3;
    if (x >= 10)
        return 2;
    if (x >= 1)
        return 1;
}
