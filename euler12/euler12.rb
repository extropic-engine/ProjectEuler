#!/usr/bin/env ruby

def triangleNumber(n)
  result = 0
  n.times do |i|
  result += (i+1)
  end
  result
end

def getDivisors(n)
  list = []
  n.times do |i|
    if (n % (i+1)) == 0
      list << (i+1)
    end
  end
  list
end

def countDivisors(n)
  getDivisors(n).length
end

def countDivisorsHeuristic(n)
  count = 2
  if (n % 2) == 0
    count += 2
  end
  for i in 3..Math.sqrt(n)
    if (n % i) == 0
      count += 2
    end
  end
  count
end


divisorCount = 0
i = 0
while divisorCount <= 500 do
  n = triangleNumber(i)
  est = countDivisorsHeuristic(n)
  puts "Triangle number #{i} has an estimated #{est} divisors."
  if est > 500
    divisorCount = countDivisors(n)
    puts "Triangle number #{i} has a confirmed #{est} divisors."
  end
  i += 1
end
puts "Answer is triangle number #{triangleNumber(i-1)}"
