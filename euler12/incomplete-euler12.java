
public class euler12 {

	
	public static void main(String[] args) {
		int number = 3;
		int i = 2;
		while (countDivisors(number) < 500) {
			i++;
			number = getNextTriangleNumber(i, number);
		}
		
		System.out.println(number + " has over 500 divisors.");
	}
	
	private static int countDivisors(int number) {
		int sum = 2;
		for (int i = 2; i < (number / 2); i++) {
			if (number % i == 0)
				sum++;
		}
		return sum;
	}
	
	private static int getNextTriangleNumber(int count, int prevTriangleNumber) {
		return prevTriangleNumber + count;
	}
}
