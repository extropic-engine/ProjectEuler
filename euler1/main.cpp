#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    int x = 0;
    for (float i = 0; i < 1000; i++)
    {
        if (((i/3) == floor(i/3)) || ((i/5) == floor(i/5)))
        {
            x+=i;
        }
    }
    cout << x << endl;
    return 0;
}
