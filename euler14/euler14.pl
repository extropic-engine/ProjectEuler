collatz(1, Length, Result) :- Result is Length + 1.
collatz(N, Length, Result) :-
    N > 1,
    0 is N mod 2,
    Nx is N / 2,
    Lx is Length + 1,
    collatz(Nx, Lx, Rx),
    Result is Rx.
collatz(N, Length, Result) :-
    N > 1,
    1 is N mod 2,
    Nx is (3 * N) + 1,
    Lx is Length + 1,
    collatz(Nx, Lx, Rx),
    Result is Rx.
