#include <stdio.h>
#include <stdlib.h>

typedef unsigned long long int u64;
const int MAX_CACHE = 262144;
u64 cache[MAX_CACHE]; // 16 mb

u64 collatz(u64 n) {
  if (n < MAX_CACHE && cache[n] != 0) {
    return cache[n] + 1;
  }

  if ((n % 2) == 0) {
    return collatz(n/2) + 1;
  } else if (n == 1) {
    return 1;
  }

  return collatz((3 * n) + 1) + 1;
}

int main() {
  const int LIMIT = 1000000;
  u64 tallest = 0;
  u64 number = 0;
  u64 x = 1; // don't bother with items checked in prev iterations

  while (x < LIMIT) {
    u64 c = collatz(x);
    if (x < MAX_CACHE) { cache[x] = c; }
    if (c > tallest)   { tallest = c; number = x; }
    if ((x % 10000) == 0) {
      printf("Crunched %llu\n", x);
    }
    x++;
  }

  printf("Tallest: %llu with %llu iterations\n", number, tallest);
}
