#!/bin/zsh

function collatz () {
    echo $cache
    if [[ -n ${(M)cache:#${1}} ]]; then return $cache[$1]; fi
    if [[ $(($1 % 2)) == 0 ]]; then
        cache[$1]=$(( `collatz $(( $1 / 2 ))` + 1 ))
    else
        cache[$1]=$(( `collatz $(( (3*$1) + 1 ))` + 1 ))
    fi
    return $cache[$1];
}

functions collatz

longest=1
longest_length=1
current=2
cache=()

x=`collatz 2`
echo $x

#while [[ current <= 1000000 ]]; do
#    current_length=collatz_length($current)
#    if [[ current_length > longest_length ]]; then
#        echo "$current has a longer collatz sequence than $longest with $current_length steps."
#        longest_length=$current_length
#        longest=$current
#    fi
#    current=$current+1
#done
#echo "$longest has the longest collatz sequence with $longest_length steps."

